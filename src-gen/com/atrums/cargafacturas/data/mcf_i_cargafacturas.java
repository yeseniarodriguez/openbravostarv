/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.cargafacturas.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity mcf_i_cargafacturas (stored in table mcf_i_cargafacturas).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class mcf_i_cargafacturas extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "mcf_i_cargafacturas";
    public static final String ENTITY_NAME = "mcf_i_cargafacturas";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_NUMDOCUMENTO = "nUMDocumento";
    public static final String PROPERTY_NUMESTABLECIMIENTO = "nUMEstablecimiento";
    public static final String PROPERTY_PUNTOEMISION = "puntoEmision";
    public static final String PROPERTY_FECHAFACTURA = "fechaFactura";
    public static final String PROPERTY_TIPOIDENTIFICACIONCLIENTE = "tipoIdentificacionCliente";
    public static final String PROPERTY_IDENTIFICACIONCLIENTE = "identificacionCliente";
    public static final String PROPERTY_NOMBRECLIENTE = "nombreCliente";
    public static final String PROPERTY_DIRECCIONCLIENTE = "direccionCliente";
    public static final String PROPERTY_EMAILCLIENTE = "emailCliente";
    public static final String PROPERTY_CODIGOPRODUCTO = "codigoProducto";
    public static final String PROPERTY_NOMBREPRODUCTO = "nombreProducto";
    public static final String PROPERTY_CANTIDAD = "cantidad";
    public static final String PROPERTY_PRECIOUNITARIO = "precioUnitario";
    public static final String PROPERTY_TOTALLINEA = "totalLinea";
    public static final String PROPERTY_IVA = "iva";
    public static final String PROPERTY_ICE = "ice";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_MENSAJE = "mensaje";
    public static final String PROPERTY_ATECOFFPROCESAR = "atecoffProcesar";
    public static final String PROPERTY_METODOPAGO = "metodoPago";
    public static final String PROPERTY_DESCRIPCION = "descripcion";

    public mcf_i_cargafacturas() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESAR, false);
        setDefaultValue(PROPERTY_ATECOFFPROCESAR, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getNUMDocumento() {
        return (String) get(PROPERTY_NUMDOCUMENTO);
    }

    public void setNUMDocumento(String nUMDocumento) {
        set(PROPERTY_NUMDOCUMENTO, nUMDocumento);
    }

    public String getNUMEstablecimiento() {
        return (String) get(PROPERTY_NUMESTABLECIMIENTO);
    }

    public void setNUMEstablecimiento(String nUMEstablecimiento) {
        set(PROPERTY_NUMESTABLECIMIENTO, nUMEstablecimiento);
    }

    public String getPuntoEmision() {
        return (String) get(PROPERTY_PUNTOEMISION);
    }

    public void setPuntoEmision(String puntoEmision) {
        set(PROPERTY_PUNTOEMISION, puntoEmision);
    }

    public Date getFechaFactura() {
        return (Date) get(PROPERTY_FECHAFACTURA);
    }

    public void setFechaFactura(Date fechaFactura) {
        set(PROPERTY_FECHAFACTURA, fechaFactura);
    }

    public String getTipoIdentificacionCliente() {
        return (String) get(PROPERTY_TIPOIDENTIFICACIONCLIENTE);
    }

    public void setTipoIdentificacionCliente(String tipoIdentificacionCliente) {
        set(PROPERTY_TIPOIDENTIFICACIONCLIENTE, tipoIdentificacionCliente);
    }

    public String getIdentificacionCliente() {
        return (String) get(PROPERTY_IDENTIFICACIONCLIENTE);
    }

    public void setIdentificacionCliente(String identificacionCliente) {
        set(PROPERTY_IDENTIFICACIONCLIENTE, identificacionCliente);
    }

    public String getNombreCliente() {
        return (String) get(PROPERTY_NOMBRECLIENTE);
    }

    public void setNombreCliente(String nombreCliente) {
        set(PROPERTY_NOMBRECLIENTE, nombreCliente);
    }

    public String getDireccionCliente() {
        return (String) get(PROPERTY_DIRECCIONCLIENTE);
    }

    public void setDireccionCliente(String direccionCliente) {
        set(PROPERTY_DIRECCIONCLIENTE, direccionCliente);
    }

    public String getEmailCliente() {
        return (String) get(PROPERTY_EMAILCLIENTE);
    }

    public void setEmailCliente(String emailCliente) {
        set(PROPERTY_EMAILCLIENTE, emailCliente);
    }

    public String getCodigoProducto() {
        return (String) get(PROPERTY_CODIGOPRODUCTO);
    }

    public void setCodigoProducto(String codigoProducto) {
        set(PROPERTY_CODIGOPRODUCTO, codigoProducto);
    }

    public String getNombreProducto() {
        return (String) get(PROPERTY_NOMBREPRODUCTO);
    }

    public void setNombreProducto(String nombreProducto) {
        set(PROPERTY_NOMBREPRODUCTO, nombreProducto);
    }

    public BigDecimal getCantidad() {
        return (BigDecimal) get(PROPERTY_CANTIDAD);
    }

    public void setCantidad(BigDecimal cantidad) {
        set(PROPERTY_CANTIDAD, cantidad);
    }

    public BigDecimal getPrecioUnitario() {
        return (BigDecimal) get(PROPERTY_PRECIOUNITARIO);
    }

    public void setPrecioUnitario(BigDecimal precioUnitario) {
        set(PROPERTY_PRECIOUNITARIO, precioUnitario);
    }

    public BigDecimal getTotalLinea() {
        return (BigDecimal) get(PROPERTY_TOTALLINEA);
    }

    public void setTotalLinea(BigDecimal totalLinea) {
        set(PROPERTY_TOTALLINEA, totalLinea);
    }

    public BigDecimal getIva() {
        return (BigDecimal) get(PROPERTY_IVA);
    }

    public void setIva(BigDecimal iva) {
        set(PROPERTY_IVA, iva);
    }

    public BigDecimal getIce() {
        return (BigDecimal) get(PROPERTY_ICE);
    }

    public void setIce(BigDecimal ice) {
        set(PROPERTY_ICE, ice);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public String getMensaje() {
        return (String) get(PROPERTY_MENSAJE);
    }

    public void setMensaje(String mensaje) {
        set(PROPERTY_MENSAJE, mensaje);
    }

    public Boolean isAtecoffProcesar() {
        return (Boolean) get(PROPERTY_ATECOFFPROCESAR);
    }

    public void setAtecoffProcesar(Boolean atecoffProcesar) {
        set(PROPERTY_ATECOFFPROCESAR, atecoffProcesar);
    }

    public String getMetodoPago() {
        return (String) get(PROPERTY_METODOPAGO);
    }

    public void setMetodoPago(String metodoPago) {
        set(PROPERTY_METODOPAGO, metodoPago);
    }

    public String getDescripcion() {
        return (String) get(PROPERTY_DESCRIPCION);
    }

    public void setDescripcion(String descripcion) {
        set(PROPERTY_DESCRIPCION, descripcion);
    }

}
