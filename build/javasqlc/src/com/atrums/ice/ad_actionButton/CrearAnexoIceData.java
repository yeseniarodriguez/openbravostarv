//Sqlc generated V1.O00-1
package com.atrums.ice.ad_actionButton;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class CrearAnexoIceData implements FieldProvider {
static Logger log4j = Logger.getLogger(CrearAnexoIceData.class);
  private String InitRecordNumber="0";
  public String codoperativo;
  public String codproductoice;
  public String graazucar;
  public String tipidentificacion;
  public String idecliente;
  public String tipventaice;
  public String vencantidad;
  public String devoluciones;
  public String prodadosbaja;
  public String fechaInicio;
  public String fechaFin;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("codoperativo"))
      return codoperativo;
    else if (fieldName.equalsIgnoreCase("codproductoice"))
      return codproductoice;
    else if (fieldName.equalsIgnoreCase("graazucar"))
      return graazucar;
    else if (fieldName.equalsIgnoreCase("tipidentificacion"))
      return tipidentificacion;
    else if (fieldName.equalsIgnoreCase("idecliente"))
      return idecliente;
    else if (fieldName.equalsIgnoreCase("tipventaice"))
      return tipventaice;
    else if (fieldName.equalsIgnoreCase("vencantidad"))
      return vencantidad;
    else if (fieldName.equalsIgnoreCase("devoluciones"))
      return devoluciones;
    else if (fieldName.equalsIgnoreCase("prodadosbaja"))
      return prodadosbaja;
    else if (fieldName.equalsIgnoreCase("fecha_inicio") || fieldName.equals("fechaInicio"))
      return fechaInicio;
    else if (fieldName.equalsIgnoreCase("fecha_fin") || fieldName.equals("fechaFin"))
      return fechaFin;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CrearAnexoIceData[] selectFecha(ConnectionProvider connectionProvider, String c_period_id)    throws ServletException {
    return selectFecha(connectionProvider, c_period_id, 0, 0);
  }

  public static CrearAnexoIceData[] selectFecha(ConnectionProvider connectionProvider, String c_period_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      " SELECT '' as codOperativo," +
      " '' as codProductoIce," +
      " '' as graAzucar," +
      " '' as tipIdentificacion," +
      " '' as ideCliente," +
      " '' as tipVentaIce," +
      " '' as venCantidad," +
      " '' as devoluciones," +
      " '' as proDadosBaja," +
      "  date(c.startdate) as fecha_inicio," +
      "  date(c.enddate) as fecha_fin" +
      "     FROM ice_genera_anexo_ice i, c_period c" +
      "     WHERE i.c_period_id = c.c_period_id" +
      "     AND i.ice_genera_anexo_ice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_period_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CrearAnexoIceData objectCrearAnexoIceData = new CrearAnexoIceData();
        objectCrearAnexoIceData.codoperativo = UtilSql.getValue(result, "codoperativo");
        objectCrearAnexoIceData.codproductoice = UtilSql.getValue(result, "codproductoice");
        objectCrearAnexoIceData.graazucar = UtilSql.getValue(result, "graazucar");
        objectCrearAnexoIceData.tipidentificacion = UtilSql.getValue(result, "tipidentificacion");
        objectCrearAnexoIceData.idecliente = UtilSql.getValue(result, "idecliente");
        objectCrearAnexoIceData.tipventaice = UtilSql.getValue(result, "tipventaice");
        objectCrearAnexoIceData.vencantidad = UtilSql.getValue(result, "vencantidad");
        objectCrearAnexoIceData.devoluciones = UtilSql.getValue(result, "devoluciones");
        objectCrearAnexoIceData.prodadosbaja = UtilSql.getValue(result, "prodadosbaja");
        objectCrearAnexoIceData.fechaInicio = UtilSql.getDateValue(result, "fecha_inicio", "dd-MM-yyyy");
        objectCrearAnexoIceData.fechaFin = UtilSql.getDateValue(result, "fecha_fin", "dd-MM-yyyy");
        objectCrearAnexoIceData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCrearAnexoIceData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CrearAnexoIceData objectCrearAnexoIceData[] = new CrearAnexoIceData[vector.size()];
    vector.copyInto(objectCrearAnexoIceData);
    return(objectCrearAnexoIceData);
  }

  public static CrearAnexoIceData[] select(ConnectionProvider connectionProvider, String ice_genera_anexo_ice_Id, String fecha_ini, String fecha_fin, String ad_client_Id)    throws ServletException {
    return select(connectionProvider, ice_genera_anexo_ice_Id, fecha_ini, fecha_fin, ad_client_Id, 0, 0);
  }

  public static CrearAnexoIceData[] select(ConnectionProvider connectionProvider, String ice_genera_anexo_ice_Id, String fecha_ini, String fecha_fin, String ad_client_Id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      " SELECT" +
      " 'ICE'as codOperativo," +
      "  ('3092'||'-'||'37'||'-'||'004394'||'-'||'070'||'-'||'000000'||'-'||'00'||'-'||'593'||'-'||'000000')as codProductoIce," +
      "  '0.00'as graAzucar," +
      "  (CASE WHEN bp.em_co_tipo_identificacion='01' THEN 'R'" +
      "        WHEN bp.em_co_tipo_identificacion='02' THEN 'C' " +
      "        WHEN bp.em_co_tipo_identificacion='03' THEN 'P'" +
      "        WHEN bp.em_co_tipo_identificacion='07' THEN 'F' END) as tipIdentificacion," +
      "  bp.taxid as ideCliente," +
      "  '1'as tipVentaIce," +
      "  round(sum(qtyinvoiced),0) as venCantidad," +
      "  '0'as devoluciones," +
      "  '0'as proDadosBaja," +
      "  '' as fecha_inicio," +
      "  '' as fecha_fin" +
      " FROM c_invoice c,ad_client cl,c_bpartner bp,c_invoiceline il,m_product p,c_tax t,c_invoicetax it,ad_orginfo oi,ad_org o,ice_genera_anexo_ice ic,c_doctype dt" +
      " WHERE c.ad_client_id=cl.ad_client_id " +
      "  AND c.c_bpartner_id=bp.c_bpartner_id" +
      "  AND c.c_invoice_id=il.c_invoice_id " +
      "  AND il.m_product_id=p.m_product_id  " +
      "  AND oi.ad_org_id=o.ad_org_id" +
      "  AND c.ad_org_id=o.ad_org_id " +
      "  AND it.c_tax_id=t.c_tax_id " +
      "  AND it.c_invoice_id=c.c_invoice_id " +
      "  AND cl.ad_client_Id=ic.ad_client_Id" +
      "  AND c.c_doctypetarget_id=dt.c_doctype_Id" +
      "  AND p.value IN('001','1')" +
      "  AND c.docstatus IN ('CO')" +
      "  AND c.issotrx='Y'" +
      "  AND em_co_tp_comp_autorizador_sri in ('1','18')" +
      "  AND t.rate =15 " +
      "  AND ic.ice_genera_anexo_ice_Id = ?" +
      "  AND date(c.dateinvoiced) >= date(?)" +
      "  AND date(c.dateinvoiced) <= date(?)" +
      "  AND c.ad_client_Id=?" +
      "  GROUP BY bp.em_co_tipo_identificacion,bp.taxid";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ice_genera_anexo_ice_Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha_ini);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha_fin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_client_Id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CrearAnexoIceData objectCrearAnexoIceData = new CrearAnexoIceData();
        objectCrearAnexoIceData.codoperativo = UtilSql.getValue(result, "codoperativo");
        objectCrearAnexoIceData.codproductoice = UtilSql.getValue(result, "codproductoice");
        objectCrearAnexoIceData.graazucar = UtilSql.getValue(result, "graazucar");
        objectCrearAnexoIceData.tipidentificacion = UtilSql.getValue(result, "tipidentificacion");
        objectCrearAnexoIceData.idecliente = UtilSql.getValue(result, "idecliente");
        objectCrearAnexoIceData.tipventaice = UtilSql.getValue(result, "tipventaice");
        objectCrearAnexoIceData.vencantidad = UtilSql.getValue(result, "vencantidad");
        objectCrearAnexoIceData.devoluciones = UtilSql.getValue(result, "devoluciones");
        objectCrearAnexoIceData.prodadosbaja = UtilSql.getValue(result, "prodadosbaja");
        objectCrearAnexoIceData.fechaInicio = UtilSql.getValue(result, "fecha_inicio");
        objectCrearAnexoIceData.fechaFin = UtilSql.getValue(result, "fecha_fin");
        objectCrearAnexoIceData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCrearAnexoIceData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CrearAnexoIceData objectCrearAnexoIceData[] = new CrearAnexoIceData[vector.size()];
    vector.copyInto(objectCrearAnexoIceData);
    return(objectCrearAnexoIceData);
  }
}
