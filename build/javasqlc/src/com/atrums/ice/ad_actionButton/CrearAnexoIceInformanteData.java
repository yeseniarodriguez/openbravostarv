//Sqlc generated V1.O00-1
package com.atrums.ice.ad_actionButton;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class CrearAnexoIceInformanteData implements FieldProvider {
static Logger log4j = Logger.getLogger(CrearAnexoIceInformanteData.class);
  private String InitRecordNumber="0";
  public String idsucursal;
  public String tipoideinformante;
  public String ideinformante;
  public String razonSocial;
  public String anio;
  public String mes;
  public String actimport;
  public String codoperativo;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("idsucursal"))
      return idsucursal;
    else if (fieldName.equalsIgnoreCase("tipoideinformante"))
      return tipoideinformante;
    else if (fieldName.equalsIgnoreCase("ideinformante"))
      return ideinformante;
    else if (fieldName.equalsIgnoreCase("razon_social") || fieldName.equals("razonSocial"))
      return razonSocial;
    else if (fieldName.equalsIgnoreCase("anio"))
      return anio;
    else if (fieldName.equalsIgnoreCase("mes"))
      return mes;
    else if (fieldName.equalsIgnoreCase("actimport"))
      return actimport;
    else if (fieldName.equalsIgnoreCase("codoperativo"))
      return codoperativo;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CrearAnexoIceInformanteData[] select(ConnectionProvider connectionProvider, String ice_genera_anexo_ice_Id)    throws ServletException {
    return select(connectionProvider, ice_genera_anexo_ice_Id, 0, 0);
  }

  public static CrearAnexoIceInformanteData[] select(ConnectionProvider connectionProvider, String ice_genera_anexo_ice_Id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT i.ad_client_Id as idsucursal" +
      "               ,'R' as TipoIdeInformante," +
      "		oi.taxid as ideInformante," +
      " 		replace(cl.name,'.','')as razon_social, " +
      " 		date_part('Year', p.startdate) as Anio," +
      " 		ltrim(to_char(date_part('Month', p.startdate),'00')) as Mes," +
      " 		'02' as actImport," +
      " 		'ICE' as codOperativo" +
      "        FROM ad_client cl,ad_orginfo oi,ice_genera_anexo_ice i,c_period p" +
      " 		WHERE cl.ad_client_Id=oi.ad_client_Id " +
      " 		AND i.c_period_Id=p.c_period_Id" +
      " 		AND cl.ad_client_Id=p.ad_client_Id " +
      " 		AND cl.ad_client_Id=i.ad_client_Id" +
      " 		AND i.ice_genera_anexo_ice_Id=? " +
      " 		limit 1   ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ice_genera_anexo_ice_Id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CrearAnexoIceInformanteData objectCrearAnexoIceInformanteData = new CrearAnexoIceInformanteData();
        objectCrearAnexoIceInformanteData.idsucursal = UtilSql.getValue(result, "idsucursal");
        objectCrearAnexoIceInformanteData.tipoideinformante = UtilSql.getValue(result, "tipoideinformante");
        objectCrearAnexoIceInformanteData.ideinformante = UtilSql.getValue(result, "ideinformante");
        objectCrearAnexoIceInformanteData.razonSocial = UtilSql.getValue(result, "razon_social");
        objectCrearAnexoIceInformanteData.anio = UtilSql.getValue(result, "anio");
        objectCrearAnexoIceInformanteData.mes = UtilSql.getValue(result, "mes");
        objectCrearAnexoIceInformanteData.actimport = UtilSql.getValue(result, "actimport");
        objectCrearAnexoIceInformanteData.codoperativo = UtilSql.getValue(result, "codoperativo");
        objectCrearAnexoIceInformanteData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCrearAnexoIceInformanteData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CrearAnexoIceInformanteData objectCrearAnexoIceInformanteData[] = new CrearAnexoIceInformanteData[vector.size()];
    vector.copyInto(objectCrearAnexoIceInformanteData);
    return(objectCrearAnexoIceInformanteData);
  }
}
