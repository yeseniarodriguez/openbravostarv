//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.cargafacturas.CargaFacturasdeVenta;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data implements FieldProvider {
static Logger log4j = Logger.getLogger(CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String mensaje;
  public String adOrgId;
  public String isactive;
  public String numDocumento;
  public String numEstablecimiento;
  public String puntoEmision;
  public String fechaFactura;
  public String tipoIdentificacionCliente;
  public String identificacionCliente;
  public String nombreCliente;
  public String direccionCliente;
  public String emailCliente;
  public String codigoProducto;
  public String nombreProducto;
  public String cantidad;
  public String precioUnitario;
  public String totalLinea;
  public String iva;
  public String ice;
  public String procesar;
  public String emAtecoffProcesar;
  public String metodoPago;
  public String descripcion;
  public String adClientId;
  public String mcfICargafacturasId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("mensaje"))
      return mensaje;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("num_documento") || fieldName.equals("numDocumento"))
      return numDocumento;
    else if (fieldName.equalsIgnoreCase("num_establecimiento") || fieldName.equals("numEstablecimiento"))
      return numEstablecimiento;
    else if (fieldName.equalsIgnoreCase("punto_emision") || fieldName.equals("puntoEmision"))
      return puntoEmision;
    else if (fieldName.equalsIgnoreCase("fecha_factura") || fieldName.equals("fechaFactura"))
      return fechaFactura;
    else if (fieldName.equalsIgnoreCase("tipo_identificacion_cliente") || fieldName.equals("tipoIdentificacionCliente"))
      return tipoIdentificacionCliente;
    else if (fieldName.equalsIgnoreCase("identificacion_cliente") || fieldName.equals("identificacionCliente"))
      return identificacionCliente;
    else if (fieldName.equalsIgnoreCase("nombre_cliente") || fieldName.equals("nombreCliente"))
      return nombreCliente;
    else if (fieldName.equalsIgnoreCase("direccion_cliente") || fieldName.equals("direccionCliente"))
      return direccionCliente;
    else if (fieldName.equalsIgnoreCase("email_cliente") || fieldName.equals("emailCliente"))
      return emailCliente;
    else if (fieldName.equalsIgnoreCase("codigo_producto") || fieldName.equals("codigoProducto"))
      return codigoProducto;
    else if (fieldName.equalsIgnoreCase("nombre_producto") || fieldName.equals("nombreProducto"))
      return nombreProducto;
    else if (fieldName.equalsIgnoreCase("cantidad"))
      return cantidad;
    else if (fieldName.equalsIgnoreCase("precio_unitario") || fieldName.equals("precioUnitario"))
      return precioUnitario;
    else if (fieldName.equalsIgnoreCase("total_linea") || fieldName.equals("totalLinea"))
      return totalLinea;
    else if (fieldName.equalsIgnoreCase("iva"))
      return iva;
    else if (fieldName.equalsIgnoreCase("ice"))
      return ice;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("em_atecoff_procesar") || fieldName.equals("emAtecoffProcesar"))
      return emAtecoffProcesar;
    else if (fieldName.equalsIgnoreCase("metodo_pago") || fieldName.equals("metodoPago"))
      return metodoPago;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("mcf_i_cargafacturas_id") || fieldName.equals("mcfICargafacturasId"))
      return mcfICargafacturasId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(mcf_i_cargafacturas.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = mcf_i_cargafacturas.CreatedBy) as CreatedByR, " +
      "        to_char(mcf_i_cargafacturas.Updated, ?) as updated, " +
      "        to_char(mcf_i_cargafacturas.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        mcf_i_cargafacturas.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = mcf_i_cargafacturas.UpdatedBy) as UpdatedByR," +
      "        mcf_i_cargafacturas.Mensaje, " +
      "mcf_i_cargafacturas.AD_Org_ID, " +
      "COALESCE(mcf_i_cargafacturas.Isactive, 'N') AS Isactive, " +
      "mcf_i_cargafacturas.NUM_Documento, " +
      "mcf_i_cargafacturas.NUM_Establecimiento, " +
      "mcf_i_cargafacturas.Punto_Emision, " +
      "mcf_i_cargafacturas.Fecha_Factura, " +
      "mcf_i_cargafacturas.Tipo_Identificacion_Cliente, " +
      "mcf_i_cargafacturas.Identificacion_Cliente, " +
      "mcf_i_cargafacturas.Nombre_Cliente, " +
      "mcf_i_cargafacturas.Direccion_Cliente, " +
      "mcf_i_cargafacturas.Email_Cliente, " +
      "mcf_i_cargafacturas.Codigo_Producto, " +
      "mcf_i_cargafacturas.Nombre_Producto, " +
      "mcf_i_cargafacturas.Cantidad, " +
      "mcf_i_cargafacturas.Precio_Unitario, " +
      "mcf_i_cargafacturas.Total_Linea, " +
      "mcf_i_cargafacturas.Iva, " +
      "mcf_i_cargafacturas.Ice, " +
      "mcf_i_cargafacturas.Procesar, " +
      "mcf_i_cargafacturas.EM_Atecoff_Procesar, " +
      "mcf_i_cargafacturas.Metodo_Pago, " +
      "mcf_i_cargafacturas.Descripcion, " +
      "mcf_i_cargafacturas.AD_Client_ID, " +
      "mcf_i_cargafacturas.MCF_I_Cargafacturas_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM mcf_i_cargafacturas" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND mcf_i_cargafacturas.MCF_I_Cargafacturas_ID = ? " +
      "        AND mcf_i_cargafacturas.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND mcf_i_cargafacturas.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data = new CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data();
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.created = UtilSql.getValue(result, "created");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.updated = UtilSql.getValue(result, "updated");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.mensaje = UtilSql.getValue(result, "mensaje");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.isactive = UtilSql.getValue(result, "isactive");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.numDocumento = UtilSql.getValue(result, "num_documento");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.numEstablecimiento = UtilSql.getValue(result, "num_establecimiento");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.puntoEmision = UtilSql.getValue(result, "punto_emision");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.fechaFactura = UtilSql.getDateValue(result, "fecha_factura", "dd-MM-yyyy");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.tipoIdentificacionCliente = UtilSql.getValue(result, "tipo_identificacion_cliente");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.identificacionCliente = UtilSql.getValue(result, "identificacion_cliente");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.nombreCliente = UtilSql.getValue(result, "nombre_cliente");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.direccionCliente = UtilSql.getValue(result, "direccion_cliente");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.emailCliente = UtilSql.getValue(result, "email_cliente");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.codigoProducto = UtilSql.getValue(result, "codigo_producto");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.nombreProducto = UtilSql.getValue(result, "nombre_producto");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.cantidad = UtilSql.getValue(result, "cantidad");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.precioUnitario = UtilSql.getValue(result, "precio_unitario");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.totalLinea = UtilSql.getValue(result, "total_linea");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.iva = UtilSql.getValue(result, "iva");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.ice = UtilSql.getValue(result, "ice");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.procesar = UtilSql.getValue(result, "procesar");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.emAtecoffProcesar = UtilSql.getValue(result, "em_atecoff_procesar");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.metodoPago = UtilSql.getValue(result, "metodo_pago");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.descripcion = UtilSql.getValue(result, "descripcion");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.mcfICargafacturasId = UtilSql.getValue(result, "mcf_i_cargafacturas_id");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.language = UtilSql.getValue(result, "language");
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.adUserClient = "";
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.adOrgClient = "";
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.createdby = "";
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.trBgcolor = "";
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.totalCount = "";
        objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[] = new CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[vector.size()];
    vector.copyInto(objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data);
    return(objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data);
  }

/**
Create a registry
 */
  public static CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[] set(String mcfICargafacturasId, String createdby, String createdbyr, String emAtecoffProcesar, String numEstablecimiento, String descripcion, String metodoPago, String adClientId, String cantidad, String iva, String identificacionCliente, String ice, String puntoEmision, String tipoIdentificacionCliente, String isactive, String emailCliente, String fechaFactura, String totalLinea, String mensaje, String procesar, String codigoProducto, String adOrgId, String direccionCliente, String updatedby, String updatedbyr, String nombreProducto, String numDocumento, String precioUnitario, String nombreCliente)    throws ServletException {
    CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[] = new CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[1];
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0] = new CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data();
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].created = "";
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].createdbyr = createdbyr;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].updated = "";
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].updatedTimeStamp = "";
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].updatedby = updatedby;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].updatedbyr = updatedbyr;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].mensaje = mensaje;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].adOrgId = adOrgId;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].isactive = isactive;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].numDocumento = numDocumento;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].numEstablecimiento = numEstablecimiento;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].puntoEmision = puntoEmision;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].fechaFactura = fechaFactura;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].tipoIdentificacionCliente = tipoIdentificacionCliente;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].identificacionCliente = identificacionCliente;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].nombreCliente = nombreCliente;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].direccionCliente = direccionCliente;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].emailCliente = emailCliente;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].codigoProducto = codigoProducto;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].nombreProducto = nombreProducto;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].cantidad = cantidad;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].precioUnitario = precioUnitario;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].totalLinea = totalLinea;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].iva = iva;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].ice = ice;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].procesar = procesar;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].emAtecoffProcesar = emAtecoffProcesar;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].metodoPago = metodoPago;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].descripcion = descripcion;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].adClientId = adClientId;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].mcfICargafacturasId = mcfICargafacturasId;
    objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0].language = "";
    return objectCargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef13EBD1B3E5E848638EFABCBF9DE05A7B_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefDC80A5CD5B2B472581C4BA1B7D1E1C87_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE mcf_i_cargafacturas" +
      "        SET Mensaje = (?) , AD_Org_ID = (?) , Isactive = (?) , NUM_Documento = (?) , NUM_Establecimiento = (?) , Punto_Emision = (?) , Fecha_Factura = TO_DATE(?) , Tipo_Identificacion_Cliente = (?) , Identificacion_Cliente = (?) , Nombre_Cliente = (?) , Direccion_Cliente = (?) , Email_Cliente = (?) , Codigo_Producto = (?) , Nombre_Producto = (?) , Cantidad = TO_NUMBER(?) , Precio_Unitario = TO_NUMBER(?) , Total_Linea = TO_NUMBER(?) , Iva = TO_NUMBER(?) , Ice = TO_NUMBER(?) , Procesar = (?) , EM_Atecoff_Procesar = (?) , Metodo_Pago = (?) , Descripcion = (?) , AD_Client_ID = (?) , MCF_I_Cargafacturas_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE mcf_i_cargafacturas.MCF_I_Cargafacturas_ID = ? " +
      "        AND mcf_i_cargafacturas.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND mcf_i_cargafacturas.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mensaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numDocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numEstablecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puntoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoIdentificacionCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, identificacionCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombreCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, direccionCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emailCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, codigoProducto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombreProducto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, precioUnitario);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalLinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, metodoPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mcfICargafacturasId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mcfICargafacturasId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO mcf_i_cargafacturas " +
      "        (Mensaje, AD_Org_ID, Isactive, NUM_Documento, NUM_Establecimiento, Punto_Emision, Fecha_Factura, Tipo_Identificacion_Cliente, Identificacion_Cliente, Nombre_Cliente, Direccion_Cliente, Email_Cliente, Codigo_Producto, Nombre_Producto, Cantidad, Precio_Unitario, Total_Linea, Iva, Ice, Procesar, EM_Atecoff_Procesar, Metodo_Pago, Descripcion, AD_Client_ID, MCF_I_Cargafacturas_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mensaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numDocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numEstablecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, puntoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoIdentificacionCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, identificacionCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombreCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, direccionCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emailCliente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, codigoProducto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombreProducto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, precioUnitario);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalLinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, metodoPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mcfICargafacturasId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM mcf_i_cargafacturas" +
      "        WHERE mcf_i_cargafacturas.MCF_I_Cargafacturas_ID = ? " +
      "        AND mcf_i_cargafacturas.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND mcf_i_cargafacturas.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM mcf_i_cargafacturas" +
      "         WHERE mcf_i_cargafacturas.MCF_I_Cargafacturas_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM mcf_i_cargafacturas" +
      "         WHERE mcf_i_cargafacturas.MCF_I_Cargafacturas_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
