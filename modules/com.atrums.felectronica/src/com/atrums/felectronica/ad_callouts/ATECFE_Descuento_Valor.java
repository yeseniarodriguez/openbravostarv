package com.atrums.felectronica.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.PriceAdjustment;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.financial.FinancialUtils;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.plm.Product;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.xmlEngine.XmlDocument;

public class ATECFE_Descuento_Valor extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  private static final BigDecimal ZERO = new BigDecimal(0.0);

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      if (log4j.isDebugEnabled())
        log4j.debug("CHANGED: " + strChanged);

      String strDescuentoValor = vars.getStringParameter("inpemAtecfeValorDesc");
      String strQtyInvoice = vars.getNumericParameter("inpqtyinvoiced");
      String strPriceActual = vars.getNumericParameter("inppriceactual");
      String strInvoiceId = vars.getStringParameter("inpcInvoiceId");

      String strPriceLimit = vars.getNumericParameter("inppricelimit");
      String strProduct = vars.getStringParameter("inpmProductId");
      String strTabId = vars.getStringParameter("inpTabId");
      String strPriceList = vars.getNumericParameter("inppricelist");
      String strPriceStd = vars.getNumericParameter("inppricestd");
      String strLineNetAmt = vars.getNumericParameter("inplinenetamt");
      String strTaxId = vars.getStringParameter("inpcTaxId");
      String strGrossUnitPrice = vars.getNumericParameter("inpgrossUnitPrice");
      String strBaseGrossUnitPrice = vars.getNumericParameter("inpgrosspricestd");
      String strtaxbaseamt = vars.getNumericParameter("inptaxbaseamt");

      try {
        if (strDescuentoValor != null)
          printPage(response, vars, strChanged, strDescuentoValor, strQtyInvoice, strPriceActual,
              strInvoiceId, strProduct, strPriceLimit, strTabId, strPriceList, strPriceStd,
              strLineNetAmt, strTaxId, strGrossUnitPrice, strBaseGrossUnitPrice, strtaxbaseamt);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strChanged,
      String strDescuentoValor, String strQtyInvoice, String strPriceActual, String strInvoiceId,
      String strProduct, String strPriceLimit, String strTabId, String strPriceList,
      String strPriceStd, String strLineNetAmt, String strTaxId, String strGrossUnitPrice,
      String strBaseGrossUnitPrice, String strTaxBaseAmt) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/felectronica/ad_callouts/CallOut").createXmlDocument();
    ATECFEDescuentoData[] data = ATECFEDescuentoData.select(this, strInvoiceId);
    String strPrecision = "0", strPricePrecision = "0";
    boolean enforcedLimit = false;
    if (data != null && data.length > 0) {
      strPrecision = data[0].stdprecision.equals("") ? "0" : data[0].stdprecision;
      strPricePrecision = data[0].priceprecision.equals("") ? "0" : data[0].priceprecision;
      enforcedLimit = (data[0].enforcepricelimit.equals("Y") ? true : false);
    }
    int StdPrecision = Integer.valueOf(strPrecision).intValue();
    int PricePrecision = Integer.valueOf(strPricePrecision).intValue();

    ATECFETaxData[] dataTax = ATECFETaxData.select(this, strTaxId, strInvoiceId);
    BigDecimal taxRate = BigDecimal.ZERO;
    Integer taxScale = new Integer(0);
    
    if (dataTax.length > 0) {
      taxRate = (dataTax[0].rate.equals("") ? new BigDecimal(1) : new BigDecimal(dataTax[0].rate));
      taxScale = new Integer(dataTax[0].priceprecision);
    }
    if (log4j.isDebugEnabled())
      log4j.debug("strPriceActual: " + strPriceActual);
    if (log4j.isDebugEnabled())
      log4j.debug("strPriceLimit: " + strPriceLimit);
    if (log4j.isDebugEnabled())
      log4j.debug("strLineNetAmt: " + strLineNetAmt);
    if (log4j.isDebugEnabled())
      log4j.debug("taxRate: " + taxRate);

    BigDecimal qtyInvoice, priceActual, lineNetAmt, descuentoValor;
    BigDecimal porcentajeDescuento;
    

    descuentoValor = new BigDecimal(strDescuentoValor);
    qtyInvoice = (!Utility.isBigDecimal(strQtyInvoice) ? ZERO : new BigDecimal(strQtyInvoice));
    
    if (!new BigDecimal(strPriceActual).equals(new BigDecimal("0.0")) && !new BigDecimal(strPriceActual).equals(new BigDecimal("0"))) {
    	porcentajeDescuento = descuentoValor.multiply(new BigDecimal("100")).divide(new BigDecimal(strPriceActual),12,
    	        BigDecimal.ROUND_HALF_EVEN).setScale(12, BigDecimal.ROUND_HALF_UP);
    }else {
    	porcentajeDescuento = new BigDecimal("0");
    }
    	
    
    priceActual = (!Utility.isBigDecimal(strPriceActual) ? ZERO : (new BigDecimal(strPriceActual)))
        .setScale(PricePrecision, BigDecimal.ROUND_HALF_UP).subtract(descuentoValor);
            
    lineNetAmt = new BigDecimal(priceActual.toString()).multiply(new BigDecimal(qtyInvoice.toString()));

    StringBuffer resultado = new StringBuffer();

    resultado.append("var calloutName='ATECFE_Descuento_Valor';\n\n");
    resultado.append("var respuesta = new Array(");

      resultado.append("new Array(\"inplinenetamt\", " + lineNetAmt.toString() + "),");
      resultado.append("new Array(\"inpemAtecfePorcentaje\", " + porcentajeDescuento.toString() + "),");
      resultado.append("new Array(\"inpemAtecfeValorDesc\", " + descuentoValor.toString() + ")");
      
    resultado.append(");");
    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
