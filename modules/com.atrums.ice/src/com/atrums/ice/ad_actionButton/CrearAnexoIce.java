package com.atrums.ice.ad_actionButton;

import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;

import org.openbravo.erpCommon.ad_actionButton.ActionButtonDefaultData;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;



/**
 * Proceso que genera los anexos del SRI segun se ha registrado en la parametrizacion.
 * 
 * @author Danie Laguasi
 * 
 */
public class CrearAnexoIce extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;
 // ConnectionProvider conn = bundle.getConnection();
  //VariablesSecureApp varsAux = bundle.getContext().toVars();
  
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strKey = vars.getStringParameter("inpiceGeneraAnexoIceId");
     // String periodo=vars.getStringParameter("inpcPeriodId");
      String strMessage = "";
      printPage(response, vars, strKey, strWindow, strProcessId, strMessage, true);
    } else if (vars.commandIn("GENERATE")) {
      String strKey = vars.getStringParameter("inpcoProcesaAnexoId");
      getPrintPage(request, response, vars, strKey);
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strProcessId, String strMessage, boolean isDefault)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: Button create file msg:" + strMessage);

    ActionButtonDefaultData[] data = null;
    String strHelp = "", strDescription = "";

    if (vars.getLanguage().equals("en_US"))
      data = ActionButtonDefaultData.select(this, strProcessId);
    else
      data = ActionButtonDefaultData.selectLanguage(this, vars.getLanguage(), strProcessId);

    if (data != null && data.length != 0) {
      strDescription = data[0].description;
      strHelp = data[0].help;
    }
    String[] discard = { "" };
    if (strHelp.equals(""))
      discard[0] = new String("helpDiscard");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/ice/ad_actionButton/CrearAnexoIce", discard)
        .createXmlDocument();
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("description", strDescription);
    xmlDocument.setParameter("help", strHelp);

    if (isDefault) {
      xmlDocument.setParameter("messageType", "");
      xmlDocument.setParameter("messageTitle", "");
      xmlDocument.setParameter("messageMessage", "");
    } else {
      OBError myMessage = new OBError();
      myMessage.setTitle("");
      if (log4j.isDebugEnabled())
        log4j.debug("CrearAnexo - before setMessage");
      if (strMessage == null || strMessage.equals(""))
        myMessage.setType("Success");
      else
        myMessage.setType("Error");
      if (strMessage != null && !strMessage.equals("")) {
        myMessage.setMessage(strMessage);
      } else
        Utility.translateError(this, vars, vars.getLanguage(), "Success");
      if (log4j.isDebugEnabled())
        log4j.debug("CrearAnexo - Message Type: " + myMessage.getType());
      vars.setMessage("CrearAnexo", myMessage);
      if (log4j.isDebugEnabled())
        log4j.debug("CrearAnexo - after setMessage");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
  private void getPrintPage(HttpServletRequest request, HttpServletResponse response,
      VariablesSecureApp vars, String strKey) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("generate " +strKey);
    StringBuffer StrXML = new StringBuffer();
    String strMessage = "";
    //----------------------------IceInformante----------------
    String idSucursal="";
    String tipoIdeInformante="";
    String identiInformante="";
    String razonSocial="";
    String anio="";
    String mes="";
    String codOperativo="";
    String actImport="";
    //----------------------------IceDetalleVenta----------------
    String codProdICE="";
    String gramoAzucar="";
    String tipoIdCliente="";
    String idCliente="";
    String tipoVentaICE="";
    //Integer ventaICE=0;
    Integer ventaICE = new Integer(0);
    String devIce="";
    String cantProdBajaICE="";
   
    try {
    	CrearAnexoIceInformanteData[] cad = CrearAnexoIceInformanteData.select(this, strKey);
    	idSucursal=cad[0].idsucursal;
    	tipoIdeInformante = cad[0].tipoideinformante;
    	identiInformante = cad[0].ideinformante;
    	razonSocial=cad[0].razonSocial;
    	anio=cad[0].anio;
    	mes=cad[0].mes;
    	actImport=cad[0].actimport;
    	codOperativo=cad[0].codoperativo;
    	
    	//------------fecha-------
    	String fechaInicio="";
    	String fechaFin="";
    	CrearAnexoIceData[] fecha = CrearAnexoIceData.selectFecha(this, strKey);
    	fechaInicio=fecha[0].fechaInicio;
    	fechaFin=fecha[0].fechaFin;
    	CrearAnexoIceData[] cad1 = CrearAnexoIceData.select(this,strKey,fechaInicio,fechaFin,idSucursal);
    	
    	if(cad1 != null && cad1.length != 0)
    	{
    	    	//-------------------cabecera--------------
      StrXML.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>\n");
      StrXML.append("<ice>\n");
      StrXML.append("<TipoIDInformante>"+tipoIdeInformante+"</TipoIDInformante>\n");
      StrXML.append("<IdInformante>"+identiInformante+"</IdInformante>\n");
      StrXML.append("<razonSocial>"+razonSocial+"</razonSocial>\n");
      StrXML.append("<Anio>"+anio+"</Anio>\n");
      StrXML.append("<Mes>"+mes+"</Mes>\n");
      StrXML.append("<actImport>"+actImport+"</actImport>\n");
      StrXML.append("<codigoOperativo>"+codOperativo+"</codigoOperativo>\n");
      StrXML.append("<ventas>\n");
      
      //----------------------------detalle-------------------------
      
    	  
      for (int i = 0; i < cad1.length; i++) {
    	  ventaICE = new Integer(cad1[i].vencantidad);
    	  StrXML.append("<vta>\n");
    	  StrXML.append("<codProdICE>"+cad1[i].codproductoice+"</codProdICE>\n");
    	  StrXML.append("<gramoAzucar>"+cad1[i].graazucar+"</gramoAzucar>\n");
    	  StrXML.append("<tipoIdCliente>"+cad1[i].tipidentificacion+"</tipoIdCliente>\n");
    	  StrXML.append("<idCliente>"+cad1[i].idecliente+"</idCliente>\n");
    	  StrXML.append("<tipoVentaICE>"+cad1[i].tipventaice+"</tipoVentaICE>\n");
    	  StrXML.append("<ventaICE>"+ventaICE+"</ventaICE>\n");
    	  StrXML.append("<devICE>"+cad1[i].devoluciones+"</devICE>\n");
    	  StrXML.append("<cantProdBajaICE>"+cad1[i].prodadosbaja+"</cantProdBajaICE>\n");
    	  StrXML.append("</vta>\n");
      }
      StrXML.append("</ventas>\n");
      StrXML.append("</ice>");
    	}
    	else
    	{
    		advisePopUp(request, response, "Error",
    	              "No Existen Ventas para ese Periodo");
    	}
    } catch (Exception e) {
      strMessage = e.getMessage();
    }
    if (!strMessage.equals("")) {
      printPage(response, vars, strKey, "", "", strMessage, false);
    } else {
      response.setContentType("application/xml");
     // response.setHeader("Content-Disposition", "attachment; filename=" + strNombreFile + ".xml");
      response.setHeader("Content-Disposition", "attachment; filename="+codOperativo+"-"+anio+"-"+identiInformante+".xml");
      PrintWriter out = response.getWriter();
      out.println(StrXML.toString());
      out.close();
    }
  }

  public String getServletInfo() {
    return "Servlet for the generation of files for ATS";
  } // end of getServletInfo() method
}
