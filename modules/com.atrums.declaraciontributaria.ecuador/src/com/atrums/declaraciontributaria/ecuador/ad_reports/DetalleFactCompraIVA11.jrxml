<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DetalleFactVentaIVA" pageWidth="1320" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="1320" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="240d0c61-61c6-4462-9af7-9cc3c9c56be2">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="388"/>
	<property name="ireport.y" value="0"/>
	<style name="style1">
		<conditionalStyle>
			<conditionExpression><![CDATA[$F{tipo_documento}.equals("APC")]]></conditionExpression>
			<style forecolor="#FF0000"/>
		</conditionalStyle>
	</style>
	<parameter name="BASE_DESIGN" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["/opt/OpenbravoMaster/modules/com.atrums.declaraciontributaria.ecuador/src"]]></defaultValueExpression>
	</parameter>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<parameter name="DateFrom" class="java.util.Date"/>
	<parameter name="DateTo" class="java.util.Date"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C:\\Users\\jorge\\Google Drive\\SVN\\SVN-estandar\\com.atrums.formatos\\src\\com\\atrums\\declaraciontributaria\\ecuador\\ad_reports\\"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[select name, num_fact, razon, fec_fact,
       sum(case when tipo_documento='APC' then subtotal_exento*(-1) else subtotal_exento end) as subtotal_exento,
       sum(case when tipo_documento='APC' then subtotal_iva0*(-1) else subtotal_iva0 end) as subtotal_iva0,
       sum(case when tipo_documento='APC' then subtotal_iva*(-1) else subtotal_iva end) as subtotal_iva,
       sum(case when tipo_documento='APC' then iva*(-1) else iva end) as iva,
       sum(case when tipo_documento='APC' then ice*(-1) else ice end) as ice,
       sum(case when tipo_documento='APC' then servicio*(-1) else servicio end) as servicio,
       sum(case when tipo_documento='APC' then otro*(-1) else otro end) as otro,
       sum(case when tipo_documento='APC' then total*(-1) else total end) as total,
       c_invoice_id,
       tipo_documento
  from(
select c_invoice_id, name, num_fact, razon, fec_fact, sum(subtotal_exento) as subtotal_exento,
       sum(subtotal_iva0) as subtotal_iva0, sum(subtotal_iva) as subtotal_iva, sum(iva) as iva,
       sum(ice) as ice, sum(servicio) as servicio, sum(otro) as otro, total,
       (case when num_ret = 1 then 1 else 0 end) as num_ret,
       num_ret_iva as num_ret_iva, sum(ret_base) as ret_base, sum(valor_ret) as valor_ret,
       tipo_documento
  from(
select  i.c_invoice_id,
        d.name, i.em_co_nro_estab||'-'||i.em_co_punto_emision||'-'||i.documentno as num_fact,
        cb.name2 as razon, date(i.dateinvoiced) as fec_fact,
        (case when tc.em_co_tp_base_imponible = '1' then it.taxbaseamt else 0 end) as subtotal_exento, -- exento
        (case when tc.em_co_tp_base_imponible = '2' then it.taxbaseamt else 0 end) as subtotal_iva0, -- iva 0%
        (case when tc.em_co_tp_base_imponible = '3' then it.taxbaseamt else 0 end) as subtotal_iva, -- -- 12% o 14%
        (case when  upper(t.name) like upper('%iva%') then it.taxamt else 0 end) as iva,
        (case when  upper(t.name) like upper('%ice%') then it.taxamt else 0 end) as ice,
        (case when  upper(t.name) like upper('%servicio%') then it.taxamt else 0 end) as servicio,
        (case when (upper(t.name) not like upper('%servicio%') and
                             upper(t.name) not like upper('%iva%') and
                             upper(t.name) not like upper('%ice%')) then it.taxamt else 0 end) as otro,
        coalesce(i.grandtotal,0) as total,
        (case when rv.documentno is not null then 1 else 0 end) as num_ret,
        (case when rvl.tipo='IVA' then 1 else 0 end) as num_ret_iva,
        (case when rvl.tipo='IVA' then sum(rvl.base_imp_retencion) else 0 end)  as ret_base,
        (case when rvl.tipo='IVA' then sum(rvl.valor_retencion) else 0 end) as valor_ret,
        coalesce(d.docbasetype,' ')as tipo_documento
     from c_invoice i
     left join c_doctype d on (i.c_doctypetarget_id=d.c_doctype_id)
     left join c_bpartner cb on (cb.c_bpartner_id = i.c_bpartner_id)
     left join c_invoicetax it on (i.c_invoice_id=it.c_invoice_id)
     left join c_tax t on (it.c_tax_id = t.c_tax_id)
     left join c_taxcategory tc on (t.c_taxcategory_id = tc.c_taxcategory_id)
     left join co_retencion_compra rv on (rv.c_invoice_id=i.c_invoice_id and
                                         rv.docstatus='CO')
     left join co_retencion_compra_linea rvl on (rv.co_retencion_compra_id=rvl.co_retencion_compra_id and rvl.tipo='IVA')
    where date(i.dateinvoiced) < date($P{DateFrom})
      and date(rv.fecha_emision) >= date($P{DateFrom})
      and date(rv.fecha_emision) <= date($P{DateTo})
      and i.issotrx='N'
      and i.docstatus = 'CO'
      and i.ad_client_id in ($P!{USER_CLIENT})
    group by i.c_invoice_id,rvl.tipo, d.name, d.docbasetype, tc.em_co_tp_base_imponible, t.name, i.grandtotal, it.taxbaseamt,
          it.taxamt, rv.documentno, cb.name2
    order by 1
    ) as datos
    group by 1,2,3,4,5,13,14,15,18
) as datos
group by 1,2,3,4,13,14
order by 1]]>
	</queryString>
	<field name="name" class="java.lang.String"/>
	<field name="num_fact" class="java.lang.String"/>
	<field name="razon" class="java.lang.String"/>
	<field name="fec_fact" class="java.sql.Date"/>
	<field name="subtotal_exento" class="java.math.BigDecimal"/>
	<field name="subtotal_iva0" class="java.math.BigDecimal"/>
	<field name="subtotal_iva" class="java.math.BigDecimal"/>
	<field name="iva" class="java.math.BigDecimal"/>
	<field name="ice" class="java.math.BigDecimal"/>
	<field name="servicio" class="java.math.BigDecimal"/>
	<field name="otro" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="c_invoice_id" class="java.lang.String"/>
	<field name="tipo_documento" class="java.lang.String"/>
	<group name="name">
		<groupExpression><![CDATA[$F{name}]]></groupExpression>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<detail>
		<band height="12" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="155" height="12" uuid="2dd81765-f8b3-4048-af8b-8f3bc0a3637a"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="335" y="0" width="145" height="12" uuid="4962777c-d18a-4d90-a2e7-49510cbd3eb5"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{num_fact}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="540" y="0" width="60" height="12" uuid="67ce169e-8baf-4f9a-bfbc-5ac12ac5180d"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_exento}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="600" y="0" width="60" height="12" uuid="19073973-45eb-46bb-8eaa-7e11894b9fd3"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_iva0}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="660" y="0" width="60" height="12" uuid="482e70ef-e468-48e7-ad2a-064858c04a27"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_iva}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="720" y="0" width="60" height="12" uuid="eed3e60a-f692-4bd8-aab8-ad47e64218c7"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{iva}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="780" y="0" width="60" height="12" uuid="f6e9bf0d-9945-41ae-b901-4728332e85c2"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ice}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="840" y="0" width="60" height="12" uuid="70367629-b877-430c-bdf2-e191df4ff7aa"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicio}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="900" y="0" width="60" height="12" uuid="5c50120b-9335-48af-b3cc-2eca31ee8297"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{otro}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="960" y="0" width="60" height="12" uuid="4c81b6e5-0a44-46c7-997f-1272af2e83a4"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<subreport>
				<reportElement x="1020" y="0" width="300" height="12" uuid="8f6df19d-9d84-4787-97c2-10c83fd88ba5"/>
				<subreportParameter name="DateTo">
					<subreportParameterExpression><![CDATA[$P{DateTo}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="DateFrom">
					<subreportParameterExpression><![CDATA[$P{DateFrom}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="DOCUMENT_ID">
					<subreportParameterExpression><![CDATA[$F{c_invoice_id}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{BASE_DESIGN} + "/com/atrums/declaraciontributaria/ecuador/ad_reports/DetalleRetencionIVAC.jasper"]]></subreportExpression>
			</subreport>
			<textField>
				<reportElement x="155" y="0" width="180" height="12" uuid="f1e015b2-cd28-4890-993e-099995517e01"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{razon}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="480" y="0" width="60" height="12" uuid="45a9b95f-9bb5-4cec-aa0b-834b533a3391"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fec_fact}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
