
package org.openbravo.erpWindows.com.atrums.cargafacturas.CargaFacturasdeVenta;




import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;

// Generated old code, not worth to make i.e. java imports perfect
@SuppressWarnings("unused")
public class CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639 extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static final String windowId = "CA65B678A1DA4CB7AD1CF1B411DE0C0C";
  private static final String tabId = "B74C45338F45474AA478D89A1729C639";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "2B661B13EA324D99BA1889670868250C";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
     List<String> explicitAccess = Arrays.asList( "");
    
     SessionInfo.setUserId(vars.getSessionValue("#AD_User_ID"));
     SessionInfo.setSessionId(vars.getSessionValue("#AD_Session_ID"));
     SessionInfo.setQueryProfile("manualProcess");
     
      if (command.contains("44F369F5CA064784B6F1256A5901ABF3")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("44F369F5CA064784B6F1256A5901ABF3");
        SessionInfo.setModuleId("01B93EBAD0474DE28A785909DD32B0E7");
      }
     
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
     

     
      if (explicitAccess.contains("44F369F5CA064784B6F1256A5901ABF3") || (securedProcess && command.contains("44F369F5CA064784B6F1256A5901ABF3"))) {
        classInfo.type = "P";
        classInfo.id = "44F369F5CA064784B6F1256A5901ABF3";
      }
     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strmcfICargafacturasId = request.getParameter("inpmcfICargafacturasId");
        
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strmcfICargafacturasId.equals(""))
              total = saveRecord(vars, myError, 'U');
          else
              total = saveRecord(vars, myError, 'I');
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {

      String strMCF_I_Cargafacturas_ID = vars.getGlobalVariable("inpmcfICargafacturasId", windowId + "|MCF_I_Cargafacturas_ID", "");
      

      String strView = vars.getSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strMCF_I_Cargafacturas_ID.equals("")) strMCF_I_Cargafacturas_ID = firstElement(vars, tableSQL);
          if (strMCF_I_Cargafacturas_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strMCF_I_Cargafacturas_ID, tableSQL);

      else printPageDataSheet(response, vars, strMCF_I_Cargafacturas_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strMCF_I_Cargafacturas_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strMCF_I_Cargafacturas_ID.equals("")) strMCF_I_Cargafacturas_ID = vars.getRequiredGlobalVariable("inpmcfICargafacturasId", windowId + "|MCF_I_Cargafacturas_ID");
      else vars.setSessionValue(windowId + "|MCF_I_Cargafacturas_ID", strMCF_I_Cargafacturas_ID);
      
      vars.setSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.view", "EDIT");

      printPageEdit(response, request, vars, false, strMCF_I_Cargafacturas_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {


      String strView = vars.getSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.view");
      String strMCF_I_Cargafacturas_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strMCF_I_Cargafacturas_ID = firstElement(vars, tableSQL);
          if (strMCF_I_Cargafacturas_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strMCF_I_Cargafacturas_ID.equals("")) strMCF_I_Cargafacturas_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strMCF_I_Cargafacturas_ID, tableSQL);

      } else printPageDataSheet(response, vars, "", tableSQL);

    } else if (vars.commandIn("RELATION")) {
      

      String strMCF_I_Cargafacturas_ID = vars.getGlobalVariable("inpmcfICargafacturasId", windowId + "|MCF_I_Cargafacturas_ID", "");
      vars.setSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.view", "RELATION");
      printPageDataSheet(response, vars, strMCF_I_Cargafacturas_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {


      printPageEdit(response, request, vars, true, "", tableSQL);

    } else if (vars.commandIn("EDIT")) {

      String strMCF_I_Cargafacturas_ID = vars.getGlobalVariable("inpmcfICargafacturasId", windowId + "|MCF_I_Cargafacturas_ID", "");
      vars.setSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strMCF_I_Cargafacturas_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {

      String strMCF_I_Cargafacturas_ID = vars.getRequiredStringParameter("inpmcfICargafacturasId");
      
      String strNext = nextElement(vars, strMCF_I_Cargafacturas_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {

      String strMCF_I_Cargafacturas_ID = vars.getRequiredStringParameter("inpmcfICargafacturasId");
      
      String strPrevious = previousElement(vars, strMCF_I_Cargafacturas_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {

      vars.setSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|MCF_I_Cargafacturas_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|MCF_I_Cargafacturas_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {

      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {

      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {

      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I');      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {

      String strMCF_I_Cargafacturas_ID = vars.getRequiredGlobalVariable("inpmcfICargafacturasId", windowId + "|MCF_I_Cargafacturas_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U');      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strMCF_I_Cargafacturas_ID, tableSQL);
          vars.setSessionValue(windowId + "|MCF_I_Cargafacturas_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {

      String strMCF_I_Cargafacturas_ID = vars.getRequiredStringParameter("inpmcfICargafacturasId");
      //CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data data = getEditVariables(vars);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.delete(this, strMCF_I_Cargafacturas_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|mcfICargafacturasId");
        vars.setSessionValue(tabId + "|CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

    } else if (vars.commandIn("BUTTONEM_Atecoff_Procesar44F369F5CA064784B6F1256A5901ABF3")) {
        vars.setSessionValue("button44F369F5CA064784B6F1256A5901ABF3.stremAtecoffProcesar", vars.getStringParameter("inpemAtecoffProcesar"));
        vars.setSessionValue("button44F369F5CA064784B6F1256A5901ABF3.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button44F369F5CA064784B6F1256A5901ABF3.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button44F369F5CA064784B6F1256A5901ABF3.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button44F369F5CA064784B6F1256A5901ABF3.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "44F369F5CA064784B6F1256A5901ABF3", request.getServletPath());
      } else if (vars.commandIn("BUTTON44F369F5CA064784B6F1256A5901ABF3")) {
        String strMCF_I_Cargafacturas_ID = vars.getGlobalVariable("inpmcfICargafacturasId", windowId + "|MCF_I_Cargafacturas_ID", "");
        String stremAtecoffProcesar = vars.getSessionValue("button44F369F5CA064784B6F1256A5901ABF3.stremAtecoffProcesar");
        String strProcessing = vars.getSessionValue("button44F369F5CA064784B6F1256A5901ABF3.strProcessing");
        String strOrg = vars.getSessionValue("button44F369F5CA064784B6F1256A5901ABF3.strOrg");
        String strClient = vars.getSessionValue("button44F369F5CA064784B6F1256A5901ABF3.strClient");

        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonEM_Atecoff_Procesar44F369F5CA064784B6F1256A5901ABF3(response, vars, strMCF_I_Cargafacturas_ID, stremAtecoffProcesar, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONEM_Atecoff_Procesar44F369F5CA064784B6F1256A5901ABF3")) {
        String strMCF_I_Cargafacturas_ID = vars.getGlobalVariable("inpKey", windowId + "|MCF_I_Cargafacturas_ID", "");
        
        ProcessBundle pb = new ProcessBundle("44F369F5CA064784B6F1256A5901ABF3", vars).init(this);
        HashMap<String, Object> params= new HashMap<String, Object>();
       
        params.put("MCF_I_Cargafacturas_ID", strMCF_I_Cargafacturas_ID);
        params.put("adOrgId", vars.getStringParameter("inpadOrgId"));
        params.put("adClientId", vars.getStringParameter("inpadClientId"));
        params.put("tabId", tabId);
        
        
        
        pb.setParams(params);
        OBError myMessage = null;
        try {
          new ProcessRunner(pb).execute(this);
          myMessage = (OBError) pb.getResult();
          myMessage.setMessage(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getMessage()));
          myMessage.setTitle(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getTitle()));
        } catch (Exception ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          log4j.error(ex);
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);





    } else if (vars.commandIn("SAVE_XHR")) {
      
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data getEditVariables(Connection con, VariablesSecureApp vars) throws IOException,ServletException {
    CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data data = new CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data();
    ServletException ex = null;
    try {
    data.mensaje = vars.getStringParameter("inpmensaje");     data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.numDocumento = vars.getRequiredStringParameter("inpnumDocumento");     data.numEstablecimiento = vars.getRequiredStringParameter("inpnumEstablecimiento");     data.puntoEmision = vars.getRequiredStringParameter("inppuntoEmision");     data.fechaFactura = vars.getRequiredStringParameter("inpfechaFactura");     data.tipoIdentificacionCliente = vars.getRequiredStringParameter("inptipoIdentificacionCliente");     data.identificacionCliente = vars.getRequiredStringParameter("inpidentificacionCliente");     data.nombreCliente = vars.getRequiredStringParameter("inpnombreCliente");     data.direccionCliente = vars.getRequiredStringParameter("inpdireccionCliente");     data.emailCliente = vars.getStringParameter("inpemailCliente");     data.codigoProducto = vars.getRequiredStringParameter("inpcodigoProducto");     data.nombreProducto = vars.getRequiredStringParameter("inpnombreProducto");    try {   data.cantidad = vars.getRequiredNumericParameter("inpcantidad");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.precioUnitario = vars.getRequiredNumericParameter("inpprecioUnitario");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalLinea = vars.getRequiredNumericParameter("inptotalLinea");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.iva = vars.getRequiredNumericParameter("inpiva");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.ice = vars.getRequiredNumericParameter("inpice");  } catch (ServletException paramEx) { ex = paramEx; }     data.procesar = vars.getStringParameter("inpprocesar");     data.emAtecoffProcesar = vars.getStringParameter("inpemAtecoffProcesar");     data.metodoPago = vars.getStringParameter("inpmetodoPago");     data.descripcion = vars.getStringParameter("inpdescripcion");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.mcfICargafacturasId = vars.getRequestGlobalVariable("inpmcfICargafacturasId", windowId + "|MCF_I_Cargafacturas_ID"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");



    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }




    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|MCF_I_Cargafacturas_ID", data[0].getField("mcfICargafacturasId"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));
    }

    private void refreshSessionNew(VariablesSecureApp vars) throws IOException,ServletException {
      CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[] data = CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), vars.getStringParameter("inpmcfICargafacturasId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strMCF_I_Cargafacturas_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    
    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strMCF_I_Cargafacturas_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strMCF_I_Cargafacturas_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/cargafacturas/CargaFacturasdeVenta/CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639", false, "document.frmMain.inpmcfICargafacturasId", "grid", "..", "".equals("Y"), "CargaFacturasdeVenta", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());



    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "mcfICargafacturasId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639_Relation.html", "CargaFacturasdeVenta", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }


    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean _boolNew, String strMCF_I_Cargafacturas_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    // copy param to variable as will be modified later
    boolean boolNew = _boolNew;

    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    
    boolean hasSearchCondition=false;
    


      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strMCF_I_Cargafacturas_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strMCF_I_Cargafacturas_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("mcfICargafacturasId") == null || dataField.getField("mcfICargafacturasId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars);
        data = CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.set("", Utility.getDefault(this, vars, "Createdby", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.selectDef13EBD1B3E5E848638EFABCBF9DE05A7B_0(this, Utility.getDefault(this, vars, "Createdby", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField)), Utility.getDefault(this, vars, "EM_Atecoff_Procesar", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "N", dataField), Utility.getDefault(this, vars, "NUM_Establecimiento", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Descripcion", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Metodo_Pago", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Cantidad", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "0", dataField), Utility.getDefault(this, vars, "Iva", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "0", dataField), Utility.getDefault(this, vars, "Identificacion_Cliente", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Ice", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "0", dataField), Utility.getDefault(this, vars, "Punto_Emision", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Tipo_Identificacion_Cliente", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), "Y", Utility.getDefault(this, vars, "Email_Cliente", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Fecha_Factura", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Total_Linea", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "0", dataField), Utility.getDefault(this, vars, "Mensaje", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Procesar", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "N", dataField), Utility.getDefault(this, vars, "Codigo_Producto", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "AD_Org_ID", "@AD_ORG_ID@", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Direccion_Cliente", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Updatedby", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.selectDefDC80A5CD5B2B472581C4BA1B7D1E1C87_1(this, Utility.getDefault(this, vars, "Updatedby", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField)), Utility.getDefault(this, vars, "Nombre_Producto", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "NUM_Documento", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField), Utility.getDefault(this, vars, "Precio_Unitario", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "0", dataField), Utility.getDefault(this, vars, "Nombre_Cliente", "", "CA65B678A1DA4CB7AD1CF1B411DE0C0C", "", dataField));
        
      }
     }
      
    
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/cargafacturas/CargaFacturasdeVenta/CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/cargafacturas/CargaFacturasdeVenta/CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpmcfICargafacturasId", "", "..", "".equals("Y"), "CargaFacturasdeVenta", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strMCF_I_Cargafacturas_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strMCF_I_Cargafacturas_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639_Relation.html", "CargaFacturasdeVenta", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));



    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
xmlDocument.setParameter("Fecha_Factura_Format", vars.getSessionValue("#AD_SqlDateFormat"));
xmlDocument.setParameter("buttonCantidad", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonPrecio_Unitario", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonTotal_Linea", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonIva", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonIce", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("EM_Atecoff_Procesar_BTNname", Utility.getButtonName(this, vars, "068C5226ED0E44A2B33BCE763B23A5E2", "EM_Atecoff_Procesar_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Atecoff_Procesar = org.openbravo.erpCommon.utility.Utility.isModalProcess("44F369F5CA064784B6F1256A5901ABF3"); 
xmlDocument.setParameter("EM_Atecoff_Procesar_Modal", modalEM_Atecoff_Procesar?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }



    void printPageButtonEM_Atecoff_Procesar44F369F5CA064784B6F1256A5901ABF3(HttpServletResponse response, VariablesSecureApp vars, String strMCF_I_Cargafacturas_ID, String stremAtecoffProcesar, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 44F369F5CA064784B6F1256A5901ABF3");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/EM_Atecoff_Procesar44F369F5CA064784B6F1256A5901ABF3", discard).createXmlDocument();
      xmlDocument.setParameter("key", strMCF_I_Cargafacturas_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "44F369F5CA064784B6F1256A5901ABF3");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("44F369F5CA064784B6F1256A5901ABF3");
        vars.removeMessage("44F369F5CA064784B6F1256A5901ABF3");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }


    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type) throws IOException, ServletException {
    CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.mcfICargafacturasId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639Data.getCurrentDBTimestamp(this, data.mcfICargafacturasId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.mcfICargafacturasId = "";
                }
                else {                    
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|MCF_I_Cargafacturas_ID", data.mcfICargafacturasId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet CargaFacturasdeVentaB74C45338F45474AA478D89A1729C639. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
